#!/usr/bin/env bash

## Prepare SSH
mkdir -p ~/.ssh
chmod 700 ~/.ssh

echo "$PRIVATE_SSH_KEY" | ssh-add - > /dev/null
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

## DEPLOY
ssh root@gitlab.janmikes.cz 'docker ps'

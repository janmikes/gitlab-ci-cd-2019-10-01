#!/usr/bin/env bash

## PREPARE SSH KEY
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo "$PRIVATE_SSH_KEY" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa && chmod 700 ~/.ssh
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config


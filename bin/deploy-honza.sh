#!/usr/bin/env bash

cd honza
git fetch
git pull --rebase
composer install --no-dev
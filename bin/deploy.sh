#!/usr/bin/env bash

mkdir -p ~/.ssh
chmod 700 ~/.ssh

echo "$PRIVATE_SSH_KEY" | ssh-add - > /dev/null

echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
ssh -o StrictHostKeyChecking=no root@gitlab.janmikes.cz 'cd /app && deploy-honza.sh'

#!/usr/bin/env bash
cd /app

git checkout $CI_COMMIT_HASH
composer install

#!/usr/bin/env bash

# /app/template-app

CI_BRANCH_NAME = rocket

cp /app/template-app /app/$CI_BRANCH_NAME

rocket.mojedomena.cz -> /app/rocket

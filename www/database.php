<?php

$host = getenv('MYSQL_HOST');
$user = getenv('MYSQL_USER');
$password = getenv('MYSQL_PASSWORD');
$database = getenv('MYSQL_DATABASE');

$mysqli = mysqli_connect($host, $user, $password, $database);
$result = $mysqli->query('SHOW TABLES');

var_dump($result->fetch_all());

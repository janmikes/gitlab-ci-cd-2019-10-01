<?php declare(strict_types=1);

function sayHello(int $name) {
	echo 'Hello' . $name;
}

sayHello('Honza Mikeš');
